import os
import sys

import numpy as np
import pandas as pd

def analyze():
    if not os.path.isfile('envie/server/data/data_store.h5'):
        prepare(pd.read_csv('envie/server/data/compiled_env_stats.csv'))

    data = fetch_data()
    return (data.pipe(filter_data)
                .pipe(engineer_features))




def prepare(raw):
    (raw.rename(columns=lambda x: '_'.join(x.lower().split()))
        .assign(time_ran=lambda x: pd.to_datetime(x['time_ran'].str.replace('EDT', '')))
        .to_hdf('envie/server/data/data_store.h5', 'data'))


def fetch_data():
    return pd.read_hdf('envie/server/data/data_store.h5', 'data')


def engineer_features(df):
     df['service_down'] = df['test_status'].apply(lambda x: x == 'FALSE')
     df['day'] = df['time_ran'].apply(lambda x: x.date())

     d = df.set_index(['environment', 'day']).sort_index()
     d['time_shift'] = d.groupby(level=[0, 1])['time_ran'].shift(-1)
     d['delta'] = d['time_shift'] - d['time_ran']
     d['hour_delta'] = (d['delta'] / np.timedelta64(1, 'h')).astype(float)
     return d.reset_index()

def filter_data(df):
    """filter data to only include business hours and external services"""
    data = df.reset_index().set_index('time_ran').sort_index()
    dg_service_mask = ~data['service_name'].str.lower().str.startswith('dg')
    filtered_data = data[dg_service_mask].between_time('9:00AM', '5:00PM')

    return filtered_data.reset_index()
