import os

from flask import Flask
from flask_restful import Api, Resource
from process_data import analyze

app = Flask(__name__)
api = Api(app)

class Data(Resource):
    def get(self):
        return analyze().head().to_json()

api.add_resource(Data, '/data')

if __name__ == '__main__':
    app.run(debug=True)
